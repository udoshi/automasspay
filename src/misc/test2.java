package misc;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;

import instarem.utilities.Constants;
import instarem.utilities.ExtentManager;
import instarem.utilities.TestUtils;
import tests.base.TestBase;

public class test2 extends TestBase{
	
	private static RemoteWebDriver driver=null;
	private String browser, url;
	private static ExtentReports testReport=null;
	private static ExtentTest extentTest;
	
	@Parameters ({"Browser", "URL"})
	@BeforeTest
	public void setup(String Browser, String URL) throws Exception
	{
		browser = Browser;
		url = URL;
		
		driver = SetBrowser(Browser);
		testReport = ExtentManager.getExtentReport();
		
		System.out.println("Browser -- > " + browser + ", URL  -- > " + url);
	}
	
	@Test(enabled=true)
	public void MytestfromTest2() throws Exception
	{
		System.out.println("Method name :" + Constants.TESTMETHODNAME);
		extentTest = ExtentManager.getTestLogger(Constants.TESTMETHODNAME);
		try
		{
			driver.get(url);
			//String signIn="id|aSignIn";
			String signIn="id|aSignIn";
			extentTest.log(LogStatus.INFO, "SIGN UP EXAMPLE---");
			extentTest.log(LogStatus.INFO, "clicked on SIGN UP");
			
			By item = TestUtils.getLocator(signIn);
			
			driver.findElement(item).click();
			Thread.sleep(5000L);
			extentTest.log(LogStatus.PASS, "Method Passed");
			//System.out.println("File generated: " + Constants.REPORTFILENAME);
		}
		catch (Exception e)
		{
			ExtentManager.getScreenshot(e.getMessage(), driver);
		}
	}
	
	@AfterTest
	public void cleanUp()
	{
		driver.quit();
		ExtentManager.WriteToReport();
	}

}
