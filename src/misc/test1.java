package misc;


import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import instarem.utilities.Constants;
import instarem.utilities.ExtentManager;
import instarem.utilities.TestUtils;
import instarem.utilities.XlsManager;
import tests.base.TestBase;

public class test1 extends TestBase{
	
	private static RemoteWebDriver driver=null;
	private String browser, url;
	private static ExtentReports testReport=null;
	private static ExtentTest extentTest;
	
	@Parameters ({"Browser", "URL"})
	@BeforeTest
	public void setup(String Browser, String URL) throws Exception
	{
		browser = Browser;
		url = URL;
		
		driver = SetBrowser(Browser);
		testReport = ExtentManager.getExtentReport();
		
		System.out.println("Browser -- > " + browser + ", URL  -- > " + url);
	}
	
	@Test(enabled=false)
	public void e2eTest_MPV2_105() throws Exception
	{
		extentTest = ExtentManager.getTestLogger(Constants.TESTMETHODNAME);
		try
		{
			driver.get(url);
			//String signIn="id|aSignIn";
			String signIn="id|aSignIn";
			extentTest.log(LogStatus.INFO, "clicked on SIGN UP");
			
			By item = TestUtils.getLocator(signIn);
			
			driver.findElement(item).click();
			Thread.sleep(5000L);
			extentTest.log(LogStatus.PASS, "Method Passed");
			
			
		}
		catch (Exception e)
		{
			ExtentManager.getScreenshot(e.getMessage(), driver);
		}
	}
	
	@DataProvider(name="ReadData")
	public Object[][] DataProvider() throws Exception 
	{		
		XlsManager xlst = new XlsManager();
		Object[][] data =xlst.GetAllDataFromSheet(Constants.TESTDATAXLS, "CreateClient");
		return data;
	}
	
	
	@Test(enabled=false)
	public void SecondScript() throws Exception
	{
		extentTest = ExtentManager.getTestLogger(Constants.TESTMETHODNAME);
		try
		{
			//String signIn="id|aSignIn";
			driver.get("https://www.google.com");
			Thread.sleep(2000L);
			
			extentTest.log(LogStatus.INFO, "Google Page Title: " + driver.getTitle());
			ExtentManager.getScreenshotPass("Google website image", driver);
			
			extentTest.log(LogStatus.PASS, "Method Passed");
			
		}
		catch (Exception e)
		{
			ExtentManager.getScreenshot(e.getMessage(), driver);
		}
	}
	
	@Test(dataProvider="ReadData", enabled=true)
	public void thirdScript(Hashtable<String, String> xlsdata)
	{
		extentTest = ExtentManager.getTestLogger(Constants.TESTMETHODNAME);
		try
		{
			extentTest.log(LogStatus.INFO, "Reading data from XLS");
			
			System.out.println(xlsdata.get("Address"));
			System.out.println(xlsdata.get("City"));
			
			assertTrue(true);
		}
		catch(Exception e)
		{
			extentTest.log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	@AfterTest
	public void cleanUp()
	{
		driver.quit();
		ExtentManager.WriteToReport();
	}
}
