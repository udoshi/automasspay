package instarem.utilities;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.microsoft.schemas.office.visio.x2012.main.CellType;
//import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import instarem.utilities.Constants;
import tests.base.TestBase;


public class XlsManager {
	
	 	private XSSFSheet sheet1 =null;
	 	private XSSFRow row   =null;
		private XSSFCell cell = null;
		private Object[][] Data=null;
		//private ExtentTest extentTest=null;
        
	    //POI is developed by Apache
	    //In framework, the test data should come from external source. Hence we use XLS for that.
	    //Test data could not be kept in script, it should be independent
	    //As on May 2017, latest version 3.16 
	    
	    //The name of the dataprovider function could be anything. In this case, given as the same method name
	    //@DataProvider(name="GetXLSData")
		
		/*public XlsManager(ExtentTest _extentT)
		{
			this.extentTest = _extentT;
		}*/
		
		
		public Object[][] GetAllDataFromSheet (String dataFileName, String sheetName) throws Exception 
		{
	        
	        //Data provider function always returns an object which is a 2 dimentional array
	        //Why object type? The returned data could be of any data type ie int, double, float, string, bytes etc.
	        Object[][] Data;
	        
	        //Create a Hashtable which will be included in COLUMN 0 of every ROW with the entire ROW content from XLS.
	        Hashtable<String,String> table=null;
	        
	        File dataFile = new File(dataFileName);
	        FileInputStream fis = new FileInputStream(dataFile);
	        
	        //Below object works with XLSX type workbook
	        //For XLS - use object of HSSFWorkbook
	        XSSFWorkbook workbook = new XSSFWorkbook(fis);
	        XSSFSheet sheet1 = workbook.getSheet(sheetName);
	        
	        if	(getCellData(0, 0, sheet1).trim().equalsIgnoreCase("blank"))
	        		System.out.println("File at Row 1: is blank" );
	        	
	        int RowCount = sheet1.getLastRowNum();
	        int colCount = sheet1.getRow(0).getLastCellNum();
	        Data = new Object[RowCount][1]; 
	        
	        //System.out.println("Total Rows are --> " + RowCount);
	        //System.out.println("Total Cols are --> " + colCount);
	        
	        for (int row=0; row<RowCount; row++)
	        {
	            table = new Hashtable<String, String>();
	            
	            for (int col=0; col<colCount; col++)
	            {
	                //Fill up the hashtable table here (Ignoring the FIRST HEADING columns row)
	                table.put(getCellData(0, col, sheet1), getCellData(row+1, col, sheet1));
	                //System.out.println(getCellData(0, col, sheet1) +"--> "+ getCellData(row+1, col, sheet1));
	            }
	            
	            //Assign the hashtable to First column of every object ROW
	            Data[row][0] = table;
	        }
	        
	        workbook.close();
	        return Data;    //Return of only the object 2D array
	    
	    }
	    
		//method to get data of one row with search option 
		public Object[][] GetRowDataFromSheet(String dataFileName, String sheetName, String reqRowValue) throws Exception {
            Object[][] Data = null;
	        
	        //Create a Hashtable which will be included in COLUMN 0 of every ROW with the entire ROW content from XLS.
	        Hashtable<String,String> table=null;
	        
	        File dataFile = new File(dataFileName);
	        FileInputStream fis = new FileInputStream(dataFile);
	        
	        XSSFWorkbook workbook = new XSSFWorkbook(fis);
	        XSSFSheet sheet1 = workbook.getSheet(sheetName);
	        
	        if	(getCellData(0, 0, sheet1).trim().equalsIgnoreCase("blank"))
	        		System.out.println("File at Row 1: is blank" );
	        
	        int RowCount = sheet1.getLastRowNum();
	        int colCount = sheet1.getRow(0).getLastCellNum();
	         
	        int row=0;
	        int row1=0;
	        for(row=0; row<RowCount; row++){
	        	String Value =getCellData(row+1, 0, sheet1);
	        	if(Value.contains(reqRowValue)){
	        	row1= row;	
	        		break;	
	        	}
	        }
	        Data = new Object[1][1];
	        table = new Hashtable<String, String>();
            
	        for (int col=0; col<colCount; col++)
            {
                //Fill up the hashtable table here (Ignoring the FIRST HEADING columns row)
                table.put(getCellData(0, col, sheet1), getCellData(row+1, col, sheet1));
                System.out.println(getCellData(0, col, sheet1) +"--> "+ getCellData(row+1, col, sheet1));
            }
            Data[0][0] = table;
            workbook.close();
            return Data;  //Return of only the object 2D array
		}
		
		public Object[][] GetXLSData(String testCaseName, String sheetName) throws Exception
		{
	        int totalRows, totalCols, startRow, endRow;
	        int row=0;
	        boolean testCaseFound=false;
	        
	        //Create a Hashtable which will be included in COLUMN 0 of every ROW with the entire ROW content from XLS.
	        Hashtable<String,String> table=null;
	        
	        File dataFile = new File(Constants.TESTDATAXLS);
	        FileInputStream fis = new FileInputStream(dataFile);
	        //ExtentManager.logInit();
	        
	        //Below object works with XLSX type workbook
	        //For XLS - use object of HSSFWorkbook
	        XSSFWorkbook workbook = new XSSFWorkbook(fis);
	        sheet1 = workbook.getSheet(sheetName);
	        totalRows = sheet1.getLastRowNum();
	        startRow=0;
	      
	        try
	        {
	        	while (!(startRow > totalRows))
	 	        {
	        		//System.out.println(getCellData(startRow, 0));
	        		
	        		if (getCellData(startRow, 0, sheet1).trim().equalsIgnoreCase(testCaseName))
	        		{
						//System.out.println("test case found");
						//ExtentManager.log4j.info("Test data found for Test case: " + testCaseName);
	        			System.out.println("Test data found for Test case: " + testCaseName);
						testCaseFound=true;
						break;
	        		}
	        		startRow++;
	 	        }
	        }
	        catch (Exception e)
	        {
	        	System.out.println("Test Data File ERROR --> " + e.getMessage());
	        }
	        
	        //System.out.println("start ROW -- > " + startRow);
	        //System.out.println("total Rows --> " + totalRows);
	        //System.out.println("Boolean --> " + testCaseFound);
	        //Finding End ROW
	        endRow=startRow+1;
	        
	        if (testCaseFound)
	        {
	        	System.out.println("Test case found");
	        	while (endRow>startRow)
	        	{
	        		System.out.println(getCellData(endRow, 0, sheet1));
	        		if (getCellData(endRow, 0, sheet1).trim().equalsIgnoreCase("end"))
	        		{
	        			//System.out.println("end found");
	        			break;
	        		}
	        		endRow++;
	        	}
	        }
	        
	        System.out.println("End Row --> " + endRow);
	        
	        try
	        {
	        	if (testCaseFound && (endRow-startRow-2)>0 && (endRow-startRow)!=1)
	 	        {
		        	Data = new Object[endRow-startRow-2][1]; 
		        	totalCols = sheet1.getRow(startRow+2).getLastCellNum();
			        
	 	        	for (int i=startRow+2; i<endRow; i++) 	//ROW
	 	        	{
	 	        		System.out.println("Printing ROW --> " + row);
 	        			
	 	        		table = new Hashtable<String, String>();
	 	   	        	for (int j=0; j<totalCols; j++)
 	        			{
 	        				if (getCellData(i, j, sheet1).trim().equalsIgnoreCase("blank"))
 		 	        		{
 		 	        			table.put(getCellData(startRow+1, j, sheet1), "");
 		 	        		}
 	        				else
 	        				{
 	        					table.put(getCellData(startRow+1, j, sheet1), getCellData(i, j, sheet1));
 	        				}
 	        			}
	 	   	        	Data[row][0] = table;
	 	   	        	row++;
	 	        	}
	 	        }
	 	        else
	 	        {
	 	        	//System.out.println("(endRow-startRow-2)" + (endRow-startRow-2));
	 	        	//System.out.println("Test case data not found OR there is no data available for test case");
	 	        	System.out.println("Test case data not found OR there is no data available for test case : " + testCaseName);
	 	        }
	        }
			catch (Exception e)
			{
				System.out.println("Issue found while reading test data: " + testCaseName +" ---> "+ e.getMessage());
			}
	        
	        workbook.close();
		    return Data;    //Return of only the object 2D array
		}
		
		
/*		@DataProvider
		public static Object[][] GetXLS(Method method) throws Exception 
		{		
			TestBase variable = new TestBase();
			
			String ExcelName = variable.getGlobalVariable("ExcelFile");
			String SheetName = variable.getGlobalVariable("SheetName");
			XlsManager xlst = new XlsManager();
			Object[][] data =xlst.GetAllDataFromSheet(Constants.TESTDATAPATH+ExcelName, SheetName);
			return data;
		}*/	
	@SuppressWarnings("resource")	
	public String getColumnValue(int rowNum, int colNum, String FileName,String SheetName)throws Exception
	{
		String Value = null;
		try
		{
			  File dataFile = new File(Constants.TESTDATAPATH);
		        FileInputStream fis = new FileInputStream(dataFile);
		        //ExtentManager.logInit();
		        
		        //Below object works with XLSX type workbook
		        //For XLS - use object of HSSFWorkbook
		       
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
		        sheet1 = workbook.getSheet(SheetName);
		        Value =getCellData(rowNum, colNum, sheet1);
		        
		      
		}
		catch(Exception e)
		{
			
		}
		
		return Value;
	}

	//==========================================================================================================================	
		
		@SuppressWarnings("deprecation")
		public String getCellData(int rowNum, int colNum, Sheet sheet1)
		{
			String data="BLANK";
			try
			{
				row = (XSSFRow) sheet1.getRow(rowNum);
				if(row==null)
					return "BLANK";
				cell = row.getCell(colNum);
				if(cell==null)
					return "BLANK";
				
				if (cell.getCellType()==Cell.CELL_TYPE_BLANK)
				{
					return "BLANK";
				}
				else if (cell.getCellType()==Cell.CELL_TYPE_STRING)
				{
					data = cell.getStringCellValue().trim();
				}
				else if (cell.getCellType()==Cell.CELL_TYPE_NUMERIC || cell.getCellType()==Cell.CELL_TYPE_FORMULA)
				{
					data = String.valueOf(cell.getNumericCellValue());
				}
				else if (cell.getCellType()==Cell.CELL_TYPE_ERROR)
				{
					return "ERROR";
				}
				return data;
			}
			catch(Exception e)
			{
				System.out.println("Data issue" + e.getMessage());
				e.printStackTrace();
			}
			return data;
		}
		TestBase timestemp = new TestBase(); 
		//Sets transaction number for the input testdataFile
		//Returns outfile
		public String getAndSetCellData(String testDataFile)
		{		
		try
		  {
			//System.out.println("File to be read --> " + TestDataFiles_mpv2.mp_TCMPV2_105_dataFile);
			File sampleFile = new File(testDataFile);
			if (!sampleFile.exists())
			{
				System.out.println("File doesnt exist: " + testDataFile);
				//extentTest.log(LogStatus.FAIL, "File doesnt exist: " + testDataFile);
			}
				
				FileInputStream excelFile = new FileInputStream(sampleFile);
				
				Workbook workbook = new XSSFWorkbook(excelFile);
	            Sheet datatypeSheet = workbook.getSheet("Transactions");
	            int totalrows = datatypeSheet.getLastRowNum();
	            //System.out.println("total Rows =--> " + totalrows);
	            
	            Row currentRow = datatypeSheet.getRow(0);
	            int totalCols = currentRow.getLastCellNum();
	            
	            int row = 0; //First row contains the column names
	            int column = 0;
	            
	            column = verifyColHeaderExists("Transaction Number", totalCols, datatypeSheet);
	            //column = verifyColHeaderExists("Equivalent Currency", totalCols, datatypeSheet);
	            
	            for (int r=(row+1); r<=totalrows; r++)
	            {
	            	//System.out.println("Trans no --> " + getCellData(r, column, datatypeSheet) );
	            	Cell cell = datatypeSheet.getRow(r).getCell(column);
	            	if (!getCellData(r, column, datatypeSheet).trim().equalsIgnoreCase("BLANK"))
	            			cell.setCellValue(timestemp.getDate()+"_"+timestemp.getRandomNo());
	            }
	            excelFile.close();
	            Thread.sleep(3000);
	            
	            String outFilename = updateFilename (sampleFile.getName());
				FileOutputStream outFile = new FileOutputStream(outFilename);
	            workbook.write(outFile);
	            workbook.close();
	            
	            //extentTest.log(LogStatus.INFO, "Returned out File : " + outFilename);
	            
	            return outFilename;
	           
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				e.printStackTrace();
				//extentTest.log(LogStatus.FAIL, "Issue during updating Sample File: " + testDataFile);
				return "";
			}
		}
		
		public int verifyColHeaderExists(String headerName, int totalCols, Sheet dataSheet)
		{
			int colHeaderNo=0;
			int row =0;
			
			try
			{
				for (int c=0; c<=totalCols; c++)
            	{
            		System.out.print(getCellData(row, c, dataSheet) + "--");
            		if (getCellData(row, c, dataSheet).trim().equalsIgnoreCase(headerName))
            		{
            			colHeaderNo = c;
            			System.out.println("Row : " + row);
            			System.out.println("col : " + colHeaderNo);
            			break;
            		}
            	}
	          
				return colHeaderNo;
			}
			catch (Exception e)
			{
				System.out.println("Issue found in verifyColHeaderExists: " + e.getMessage());
				return colHeaderNo=-1;
			}
		}
		

		
		public String updateFilename(String oldFilename)
		{
			String updatedFilename="";
			
			if (oldFilename=="")
				System.out.println("Old File name is blank: " + oldFilename);
			
			String[] splitString = oldFilename.split("\\.");
			//System.out.println(splitString.length);
			//System.out.println(splitString[0]);
			
			updatedFilename = "E2E_" + timestemp.getDate() + "_" + timestemp.getTime() + "." + splitString[1];
			updatedFilename = Constants.PROJPATH + "\\data\\" + updatedFilename;
			
			//System.out.println(updatedFilename);
			
			return updatedFilename;
			
		}
		
		/*public boolean setCellData(String sheetName, int col, int rowNum, String data)
		{
			//System.out.println("setCellData setCellData******************");
			try
			{
				FileInputStream fis = new FileInputStream(TestDataFiles_mpv2.mp_TCMPV2_105_dataFile); 
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
	
				if(rowNum<=0)
					return false;
				
				int index = workbook.getSheetIndex(sheetName);
				int colNum=-1;
				if(index==-1)
					return false;
				
				XSSFSheet sheet = workbook.getSheetAt(index);
			
			cell = row.getCell(col);	
			if (cell == null)
		        cell = row.createCell(col);
				
		    cell.setCellValue(data);
		    
		    XSSFCreationHelper createHelper = workbook.getCreationHelper();

		    //cell style for hyperlinks
		    //by default hypelrinks are blue and underlined
		    CellStyle hlink_style = workbook.createCellStyle();
		    XSSFFont hlink_font = workbook.createFont();
		    hlink_font.setUnderline(XSSFFont.U_SINGLE);
		    hlink_font.setColor(IndexedColors.BLUE.getIndex());
		    hlink_style.setFont(hlink_font);
		    //hlink_style.setWrapText(true);

		    XSSFHyperlink link = createHelper.createHyperlink(XSSFHyperlink.LINK_FILE);
		    link.setAddress(url);
		    cell.setHyperlink(link);
		    cell.setCellStyle(hlink_style);
		      
		    FileOutputStream fileOut = new FileOutputStream(TestDataFiles_mpv2.mp_TCMPV2_105_dataFile);
			workbook.write(fileOut);

		    fileOut.close();	

			}
			catch(Exception e){
				e.printStackTrace();
				return false;
			}
			return true;
		}*/
		
		
			
		/*@DataProvider(name="GetXLSData")
		public Object[][] DataProvider1() throws Exception 
		{		   
			return GetXLSData("TestCASE2", "Sheet1");
		}
	
	    @Test(dataProvider="GetXLSData")
	    public void displayDate(Hashtable<String, String> xlsData){
	    	try
	    	{
	    		System.out.println(xlsData.get("username"));
	    		System.out.println(xlsData.get("password"));
	    		System.out.println(xlsData.get("runmode"));
	    		
	    		System.out.println(xlsData.get("Col1"));
	    		System.out.println(xlsData.get("Col2"));
	    		System.out.println(xlsData.get("Col3"));
	    		
	    		System.out.println(xlsData.get("firstname"));
	    		System.out.println(xlsData.get("lastname"));
	    		System.out.println(xlsData.get("purpose"));
	    	}
	    	catch (NullPointerException e)
	    	{
	    		System.out.println("NULL data found");
	    	}
	    }*/
		public int getRowContains(String fileName, String textString, int colNum) throws Exception{

    		int RowIndex=-1;

    		try {
    			  File sampleFile = new File(fileName);
    			  if (!sampleFile.exists())
    				{
    					System.out.println("File doesnt exist: " + fileName);
    					//extentTest.log(LogStatus.FAIL, "File doesnt exist: " + testDataFile);
    				}
    				
    			FileInputStream excelFile = new FileInputStream(fileName);
  				
  				Workbook workbook = new XSSFWorkbook(excelFile);
  	            Sheet datatypeSheet = workbook.getSheet("Config");
    					int rowCount = datatypeSheet.getLastRowNum();

            			for (int i=1 ; i<=rowCount; i++){
            				//System.out.println("In for Loop "+i+" for textString: "+textString);
            				if  (getCellData(i,colNum,datatypeSheet).equalsIgnoreCase(textString)){
            					//writeLog.infoLog("Row found for text String: "+textString);
            					RowIndex=i;
            					//System.out.println("RowIndex 1: "+RowIndex+" and i:"+i+" for text string: "+textString);
            					//return RowIndex;
            					break;

            				}

            			}
    					
    				
    					
    			 }
    			
    			//System.out.println("RowIndex 2: "+RowIndex+" for text string: "+textString);
    			//writeLog.infoLog("Row Number Return as: "+RowIndex+" for text string: "+textString);
    			

    				catch (Exception e){

    			throw(e);

    			}
			return RowIndex;

    		}
	

}
