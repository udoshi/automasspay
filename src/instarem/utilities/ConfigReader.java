package instarem.utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.testng.Reporter;

public class ConfigReader {
	
	public static Properties properties;

	
	public static void ConfigFileReader() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(Constants.propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Configuration.properties not found at " + Constants.propertyFilePath);
		}
	}


	public static String ConfigFileValue(String ParameterName){
		String Value = null;
		try{
			ConfigFileReader();
			Value =properties.getProperty(ParameterName);
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("Exception in ConfigFileValue() : " + e.getMessage());
		}
		return Value;
	}
}
