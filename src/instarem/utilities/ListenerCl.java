package instarem.utilities;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

public class ListenerCl implements IInvokedMethodListener {
	
	public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) 
	{
		Constants.TESTMETHODNAME = returnMethodName(arg0.getTestMethod());
		//String textMsg = "About to begin executing following method : " + returnMethodName(arg0.getTestMethod());
		//System.out.println(textMsg);
		//Reporter.log(textMsg, true);
	}
	
	private String returnMethodName(ITestNGMethod method)
	{
		return method.getRealClass().getSimpleName() + "." + method.getMethodName();
	}

	public void afterInvocation(IInvokedMethod arg0, ITestResult arg1) 
	{
		//TODO Auto-generated method stub
		String textMsg = "Completed executing following method : " + returnMethodName(arg0.getTestMethod());
		System.out.println(textMsg );
		
		System.out.println("Result --> " + arg1.getStatus());
		//Reporter.log(textMsg, true);
	}
}
