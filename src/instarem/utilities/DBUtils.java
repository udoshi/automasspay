package instarem.utilities;

import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
//import com.sun.org.apache.bcel.internal.generic.RETURN;

import pages.base.BasePage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DBUtils {
	String query3;
	String firstName = null;

	/*
	 * Created a function to create connection with DB Get Remitter ID and OTP
	 * Created by UNIK DOSHI on 13-03-2018
	 */
	private static ExtentTest extentTest = null;
	public static String FxRateQuery = "select * from currency_pair where source_currency = '\" + SourceCurrency + \"' and destination_currency = '\" + DestinationCurrency + \"';";
	private RemoteWebDriver pageDriver = null;
	BasePage BasePage = new BasePage(pageDriver, extentTest, "DBUtils");
	public static int getRemitterId(String UserEmail) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int otp = 0;
		int remitter_id = 0;
		try {
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(
					"select * from remitter where user_id = (select user_id from user_account where email ='"
							+ UserEmail + "');");
			if (rs.next()) {
				String remitter = rs.getString("remitter_id");
				remitter_id = Integer.parseInt(remitter);
				System.out.println(remitter_id);
			} else {
				remitter_id = 0;
			}
		} catch (Exception e) {
			extentTest.log(LogStatus.INFO, e + "Remitter does not found");
		}
		return remitter_id;
	}

	public static String getOtp(String UserEmail) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String otp = null;
		try {
			// Setting DB connection with credentils
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			int remitter_id = getRemitterId(UserEmail);
			rs = stmt.executeQuery(
					"select * from otp_request where remitter_id = '" + remitter_id + "'ORDER BY updated_at desc;;");
			if (rs.next()) {
				otp = rs.getString("request_otp");
				System.out.println(otp);
			} else {
				otp = null;
			}
		} catch (Exception e) {
			extentTest.log(LogStatus.INFO, e + "OTP not found for given remitter");
		}
		return otp;
	}

	
	public static double getFxRate(String SourceCurrency, String DestinationCurrency) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		double FXRate = 0.0000;
		try {
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select * from currency_pair where source_currency = '" + SourceCurrency + "' and destination_currency = '" + DestinationCurrency + "';");
			if (rs.next()) {
				FXRate = rs.getDouble("rate");
				System.out.println(FXRate);
			} else {
				FXRate = 0.0000;
			}
		} catch (Exception e) {
			extentTest.log(LogStatus.INFO, e + "FX rate not found for currency pair");
		}
		return FXRate;

	}
	
	public static void main(String[] args){
		double value = getFxRate("AUD", "INR");
		System.out.println(value);
	}

	public static HashMap<String, Float> getTransactionConfig(String SourceCurrency, String DestinationCurrency,
			String accountType) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		float marginRate = 0.0000f;
		HashMap<String, Float> map = new HashMap<String, Float>();
		try {
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			rs1 = stmt.executeQuery("select * from currency where code = '"+DestinationCurrency+"';");
			if(rs1.next()){
			boolean value = rs1.getBoolean("is_active");	
			if(value==true){
				map.put("IsActive", (float) 1);
			}
			else{
				map.put("IsActive", (float) 0);
			}
			}
			rs = stmt.executeQuery("SELECT * from transaction_config where country_currency_id =(Select country_currency_id from country_currency where source_currency = '"
							+ SourceCurrency + "' and destination_currency = '" + DestinationCurrency
							+ "' Limit 1) and account_type = '" + accountType + "'AND tier =  'SILVER';");
			if (rs.next()) {
				map.put("MarginPercent", rs.getFloat("margin_percent"));
				map.put("TransactionFeePercent", rs.getFloat("transaction_fee_percent"));
				map.put("TransactionFeeFixedAmount", rs.getFloat("transaction_fee_fixed_amount"));
				map.put("MarginFixedAmount", rs.getFloat("margin_fixed_amount"));
				map.put("MinSourceAmountLimit", rs.getFloat("min_source_amount_limit"));
				
			} else {
				marginRate = 0.0000f;
			}
		} catch (Exception e) {
			extentTest.log(LogStatus.INFO, e + "Rate not found for currency pair");
		}
		return map;

	}
	
	public static String getResetLink(String UserName) {
		String Email = null;
		String Time = null;
		String URL = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try
		{	
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();	
			rs = stmt.executeQuery("select * from user_account where email ILIKE '"+UserName+"';");
			if (rs.next()) {
			  URL = rs.getString("reset_password_link");
			  System.out.println(URL);
					
				}
		}
		catch(Exception e)
		{
			extentTest.log(LogStatus.INFO, e + "Reset linked not fetched properly");
		}
		return URL;
	}
	
	public static String getTransactionNumber(String UserEmail){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String TrxnNumber = null;
		try{
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select * from transaction INNER JOIN remitter on transaction.remitter_id = remitter.remitter_id INNER JOIN user_account a ON remitter.user_id = a.user_id where email = '"+UserEmail+"' ORDER BY a.created_at DESC");
			if (rs.next()) {
				TrxnNumber = rs.getString("transaction_number");
				System.out.println(TrxnNumber);
			}
		}
		catch(Exception e){
			extentTest.log(LogStatus.INFO, e + "OTP not found for given remitter");
		}
		return TrxnNumber;
	}
	
	public static String userEmail(String UserStatus){
		String Email = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select * from user_account INNER JOIN remitter on user_account.user_id = remitter.user_id where status = '"+UserStatus+"' and remitter.is_phone_verified = 'True' and user_account.auth_type = 'BASIC' and email ILIKE '11%' and remitter.country_code IN ('AU','SG') ORDER BY user_account.created_at ASC;");
			if(rs.next()){
				Email = rs.getString("email");
				System.out.println(Email);
			}
		}
		catch(Exception e){
			extentTest.log(LogStatus.INFO, e + "Rate not found for currency pair");
		}
		return Email;
	}
	
	public static String regUserEmail(String UserStatus){
		String Email = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select  status, email, * from user_account where status = '"+UserStatus+"' and client_id=6 and auth_type!='BASIC'");
			if(rs.next()){
				Email = rs.getString("email");
				System.out.println(Email);
			}
		}
		catch(Exception e){
			extentTest.log(LogStatus.INFO, e + "Rate not found for currency pair");
		}
		return Email;
	}
	
	public static void setResetLink(String UserName, String Link) {
		String Email = null;
		String Time = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int affectedrows = 0;
		
		try
		{
			
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("EE MMM dd HH:mm:ss zzz yyyy");
				calendar.getTime();
				calendar.add(Calendar.MINUTE, 5);
				calendar.getTime();
				String formattedDate= simpleDateFormat.format(calendar.getTime());
		    
				
			String SQL = "UPDATE user_account SET reset_password_link = '"+Link+"',reset_password_link_expires_at ='"+formattedDate+"' where email = '" + UserName + "';";
			try (PreparedStatement pstmt = conn.prepareStatement(SQL)) {
				affectedrows = pstmt.executeUpdate();
			}	
		}
		catch(Exception e)
		{
			extentTest.log(LogStatus.INFO, e + "Reset linked not set properly");
		}
		
	}

	public static String activateUser(String UserName) {
		Connection conn = null;
		Statement stmt = null;
		String UserStatus = null;
		ResultSet rs = null;
		String Value = null;
		try {
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			int affectedrows = 0;
			String SQL = "UPDATE user_account SET status = 'APPROVED' where email = '" + UserName + "';";
			try (PreparedStatement pstmt = conn.prepareStatement(SQL)) {
				affectedrows = pstmt.executeUpdate();
				rs = stmt.executeQuery("select * from user_account where email = '" + UserName + "';");
				if (rs.next()) {
					UserStatus = rs.getString("status");
					if (UserStatus.contains("APPROVED")) {
						Value = "Updated";
					} else {
						Value = "User Status not approved";
					}
				}
			}
		} catch (Exception e) {
			extentTest.log(LogStatus.INFO, e + "Contact details not found for given client");
		}
		return Value;
	}

	public static String getContactDetails(String CountryCode) {
		Connection conn = null;
		Statement stmt = null;
		int CountryMobileCode = 0;
		String CountryMobileNumber = null;
		String ContactDetails = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM client WHERE '" + CountryCode + "' = ANY(country_code);");
			if (rs.next()) {
				CountryMobileCode = rs.getInt("mobile_country_code");
				CountryMobileNumber = rs.getString("mobile_number");
				ContactDetails = "+" + CountryMobileCode + CountryMobileNumber;
				System.out.println(ContactDetails);
			}

			else {
				ContactDetails = null;
			}

		} catch (Exception e) {
			extentTest.log(LogStatus.INFO, e + "Contact details not found for given client");
		}
		return ContactDetails;
	}
	
	public String myInfoDetails(String query, String email, String columnName, String value) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String val = null;
		String value1 = null;

		ArrayList<Object> list2 = null;

		String value2 = null;
		String value3 = "";
		String valuep = null;
		String dbFirstName = null;

		String query1 = "select * from remitter_corporate where remitter_id = (select remitter_id from remitter where user_id = (select user_id from user_account where email = '"
				+ email + "'))";
		String query2 = "SELECT * FROM instarem_object WHERE instarem_object_id=" + value;
		String query4 = "select count(0) from remitter_corporate_representative where remitter_id = (select remitter_id from remitter where user_id = (select user_id from user_account where email = '"
				+ email + "'))";
		String query5 = "select * from remitter_corporate_representative where remitter_id = (select remitter_id from remitter where user_id = (select user_id from user_account where email = '"
				+ email + "'))";
		String query7 = "select * from user_account where user_id in (select user_id from remitter where account_type = 'CORPORATE' and country_code = '"+value+"') and status = 'REGISTERED'and auth_type = 'BASIC' and email like '%AUTO_%';";
		String query8 = "select count(0) from remitter_corporate_representative where remitter_id = (select remitter_id from remitter where user_id = (select user_id from user_account where email = '"
				+ email + "'))";
		String query9 = "select * from remitter where user_id = (select user_id from user_account where email = '"+email+"');";
		try {
			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();

			switch (query) {
			case "query1":
				rs = stmt.executeQuery(query1);
				break;

			case "query2":
				rs = stmt.executeQuery(query2);
				break;

			case "query4":
				rs = stmt.executeQuery(query4);
				break;
			
			case "query7":
				rs = stmt.executeQuery(query7);
				break;
			
			case "query8":
				rs = stmt.executeQuery(query8);
				break;
				
			case "query9":
				rs = stmt.executeQuery(query9);
				break;
			
			}

			if (query.contains("query1") || query.contains("query2") || query.contains("query4") || query.contains("query7")
					|| query.contains("query8") || query.contains("query9"))
				if (rs.next()) {
					val = rs.getString(columnName);
					//System.out.println(val);

				}

			if (query.contains("query6")) {
				if (columnName.contains("email")) {
					rs = stmt.executeQuery(
							"select * from remitter_corporate_representative where  email = '" + email + "'");
					while (rs.next()) {
						val = rs.getString(columnName);
					}

				} else {
					String query6 = "select * from remitter_corporate where remitter_id =(select remitter_id from remitter where user_id = (select user_id from user_account where email = '"
							+ email + "'));";
					rs = stmt.executeQuery(query6);
					while (rs.next()) {
						val = rs.getString(columnName);
					}
				}
			}

			if (query.contains("query5")) {
				rs = stmt.executeQuery(query5);

				while (rs.next()) {
					val = rs.getString(columnName);
					//System.out.println(val);
					if (val.equalsIgnoreCase(value)) {
						firstName = val;
						break;
					}
				}
			}
			//System.out.println("akdhaskdshakaskd" + firstName);

			if (query.contains("address")) {
				//System.out.println(value);
				rs = stmt.executeQuery(query5);
				while (rs.next()) {
					valuep = rs.getString("first_name");
					if (valuep.equalsIgnoreCase(value)) {
						//System.out.println(valuep);
						break;
					}
				}
				String queryt = "select * from remitter_corporate_representative where remitter_id = (select remitter_id from remitter where user_id = (select user_id from user_account where email = '"
						+ email + "')) and first_name ='" + valuep + "'";
				//System.out.println(queryt);
				rs = stmt.executeQuery(queryt);
				while (rs.next()) {
					val = rs.getString(columnName);
					//System.out.println(val);
				}
				return val;

			}

			if (query.contains("query3")) {
				//System.out.println(value);
				if (value.contains("authorities")) {

					//System.out.println("query 5 is " + query5);
					rs = stmt.executeQuery(query5);

					while (rs.next()) {
						valuep = rs.getString("first_name");
						//System.out.println(valuep);
						//System.out.println(firstName);
						if (valuep.contains(firstName)) {
							dbFirstName = valuep;
							break;
						}

					}
					//System.out.println(dbFirstName);
					String queryt = "select * from remitter_corporate_representative where remitter_id = (select remitter_id from remitter where user_id = (select user_id from user_account where email = '"
							+ email + "')) and first_name ='" + dbFirstName + "'";
					//System.out.println(queryt);
					rs = stmt.executeQuery(queryt);
					if (rs.next()) {
						value1 = rs.getString("remitter_corporate_representative_id");
						//System.out.println(value1);
					}

					String queryio = "select * from remitter_corporate_representative_mapping where remitter_corporate_representative_id = '"
							+ value1 + "'";
					//System.out.println(queryio);
					rs = stmt.executeQuery(queryio);

					/*
					 * for (int o = 0; o < list1.size(); o++) { String queryx =
					 * "select * from remitter_corporate_representative_mapping where remitter_corporate_representative_id ="
					 * + list1.get(o); //System.out.println(queryx); rs =
					 * stmt.executeQuery(queryx);
					 */ list2 = new ArrayList<>();
					while (rs.next()) {
						value2 = rs.getString("remitter_corporate_entity_id");
						//System.out.println(value2);
						list2.add(value2);
					}

					////System.out.println(list2.size());
					
					for (int h = 0; h < list2.size(); h++) {
						String queryz = "select * from instarem_object where instarem_object_id =" + list2.get(h);
						//System.out.println(queryz);
						rs = stmt.executeQuery(queryz);

						while (rs.next()) {
							value2 = rs.getString("description");
							//System.out.println(value2);
							value3 = value3 + value2 + ", ";
							//System.out.println(value3);

						}

					}
					int nn = value3.length();
					//System.out.println(nn);
					value3 = value3.substring(0, nn - 2).toLowerCase();
					//System.out.println(value3);


					val = value3;

				} else {
					//System.out.println(firstName);
					query3 = "select * from remitter_corporate_representative where remitter_id = (select remitter_id from remitter where user_id = (select user_id from user_account where email = '"
							+ email + "' AND first_name = '" + firstName + "'))";
					//System.out.println(query3);
					rs = stmt.executeQuery(query3);
					if (rs.next()) {
						val = rs.getString(columnName);
						//System.out.println(val);
					}
				}

			}
			return val;
		} catch (Exception e) {
			extentTest.log(LogStatus.INFO, e + "Issue in value fetching from DB");
			return val;
		}
	}

	public String object1(String value, String columnName) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String val = null;

		String query2 = "SELECT * FROM instarem_object WHERE instarem_object_id=" + value;

		try {

			conn = DriverManager.getConnection(Constants.JDBC_URL, Constants.DB_Username, Constants.DB_Password);
			stmt = conn.createStatement();
			System.out.println(query2);
			rs = stmt.executeQuery(query2);

			// rs = stmt.executeQuery("select * from
			// remitter_corporate_representative where remitter_id = (select
			// remitter_id from remitter where user_id = (SELECT user_id FROM
			// user_account where email = 'AUTO_AU_0504172034@yopmail.com')) and
			// first_name = 'Aristo'");
			if (rs.next()) {
				val = rs.getString(columnName);
				System.out.println(val);
			}
			return val;
		} catch (Exception e) {
			extentTest.log(LogStatus.INFO, e + "Issue in value fetching from DB");
			return val;
		}

	}	
}