package instarem.utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.ITestNGMethod;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Constants {
	
	//Listener data
	public static String TESTMETHODNAME;
	public static List<ITestNGMethod> PASSEDTESTS = new ArrayList<ITestNGMethod>();
	public static List<ITestNGMethod> FAILEDTESTS = new ArrayList<ITestNGMethod>();
	public static List<ITestNGMethod> SKIPPEDTESTS = new ArrayList<ITestNGMethod>();
	
	//Project Path
	public static final String PROJPATH =  System.getProperty("user.dir");
	
	//TestData EXCEL
	public static final String TESTDATAXLS = System.getProperty("user.dir") + "//data//TestData.xlsx";
	public static final String TESTDATAPATH = System.getProperty("user.dir") + "//data//";
	public static final String CONFIGFILE = System.getProperty("user.dir") + "//data//Config.xlsx";
	
	public static final String Old_B2CPassword = "12345";
	
	//Log4J 
	public static final String LOGFILE = PROJPATH + "/Resources/log4j.xml";
	
	//DB credentials
    public static final String JDBC_URL = "jdbc:postgresql://192.168.2.248:5432/instarem_development";
    public static final String DB_Username = "instarem_admin";
    public static final String DB_Password = "InstaremAdmin1!";
	
	//Instarem Excel Path
    public static final String INSTAREMTESTDATAXLS = System.getProperty("user.dir") + "//data//InstaremV2.xlsx";
    
    public static final String UPLOADFILEPATH = System.getProperty("user.dir") + "//data//UploadFiles//";
    public static final String DOWNLOADFILE = "C://Users//unikdoshi//Downloads";
    //HUB url
	public static final String SELENIUM_GRID_URL = "http://192.168.2.234:4444/wd/hub"; 
	
	//URLS for Masspay menu/sub menu options
	public static final String MP_LOGIN = "login";
	public static final String MP_UPLOADTRANSACTIONS = "transactions/uploadtransactions";
	public static final String MP_BATCHUPLOADAPPR = "transactions/batchapproval";
	public static final String MP_COMPLIANCERFI = "transactions/compliancerfi";
	public static final String MP_ACCOUNTSTATEMENT = "reports/accountstatement";
	public static final String MP_TRXNHISTORY = "reports/transactionshistory";
	
	//Page load time (IN SECONDS)
	public static final int PAGEREFRESHDURATIONSHORT = 10; //in Seconds
	public static final int PAGEREFRESHDURATIONLONG = 30;
	public static final int ELEMENTWAITDURATIONSHORT = 15; //in Seconds
	public static final int ELEMENTWAITDURATIONLONG = 30; //in Seconds
	public static final String BaseURL = "http://192.168.2.241:3030/";
	//Config file path
	public static final String propertyFilePath= System.getProperty("user.dir") + "//Configuration.properties";
	//Images Path - ERROR Images
	public static final String IMAGESPATH = System.getProperty("user.dir") + "//Images//";
	
	//Reports Path - Reports
	public static final String EXTENTCONFIGPATH = PROJPATH + "//extent-config.xml";
	public static final String REPORTPATH = System.getProperty("user.dir") + "//Reports//";
	public static String REPORTFILENAME="";
	
	//Driver Paths
	public static final String CHROMEDRIVERPATH = PROJPATH + "//drivers//chromedriver.exe";//its 2.36 version of chromedriver
	public static final String EDGEDRIVERPATH = PROJPATH + "//drivers//MicrosoftWebDriver.exe";
	public static final String FIREFOXDRIVERPATH = PROJPATH + "//drivers//geckodriver_32bit_0.19.exe";
	//public static final String ffBinaryPath = "C://Users//Priya//AppData//Local//Mozilla Firefox//firefox.exe";
	public static final String LINUXCHORMEDRIVERPATH = PROJPATH + "//drivers//chromedriver"; //V 2.36 - 64Bit linux
	public static final String IEDRIVERPATH = PROJPATH + "//drivers//IEDriverServer.exe";
	public static final String JSONFILEPATH = PROJPATH + "//data//GlobalVariables.json";
	public static final String DATAFILEPATH = PROJPATH + "//data//TestData.xlsx";
	public static final String LOGFILEPATH = PROJPATH + "//Reports//log.txt";
	
	//Masspay AUTO USERS
	public static final String CLIENTNAME = "Autoclientonly";
	public static final String MPUSER_COMPLIANCE = "pthakur+29@instarem.com"; //Compliance RFI
	public static final String MPUSER_FINCHECKER = "pthakur+28@instarem.com";
	public static final String MPUSER_FINMAKER = "pthakur+27@instarem.com";
	public static final String MPUSER_OPERATIONS = "pthakur+26@instarem.com"; //Reports only
	public static final String MPUSER_SYSADMIN = "pthakur+25@instarem.com"; //Batch upload, Batch approval
	public static final String MPUSER_ALLRIGHTS = "pthakur+30@instarem.com";//All rights
	public static final String MP_PASSWORD = "Smart100*";
	public static final String MP_ACCESSCODE = "123456";
	public static final String User_email = "masspayinsta@gmail.com";
	public static final String ACH_BankUserName = "user_good";
	public static final String ACH_BankPassword = "pass_good";
	
	public static final String User_Password = "Abc@1234";
	
	
	//Instarem AUTO USERS
	public static final String B2C_ADMINUSER = "autoadmin@instarem.com"; 
	public static final String B2C_INDI_USER = "autoindi@instarem.com";	//AUD profile
	public static final String B2C_CORP_USER = "autocorp@instarem.com";
	public static final String B2C_PASSWORD = "Abc@123";
	
	//Instarem MENU PATHS - COMPLIANCE 
	public static final String COMPLIANCE_INITIALDOCVERFICATION  = "AdminSection/InitialDocumentVerification.aspx";
	
	public static final String timeStamp = new SimpleDateFormat("MMddHHmmss").format(new Date());
	
}
