package tests.masspay;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import instarem.utilities.ConfigReader;
import instarem.utilities.Constants;
import instarem.utilities.ExtentManager;
import tests.base.TestBase;

public class Test2 extends TestBase {
	
	private static String browser, url;
	private static RemoteWebDriver driver=null;
	private static ExtentReports testReport=null;
	private static ExtentTest extentTest=null;
	
	
	@BeforeTest(alwaysRun=true)
	public void setup() throws Exception
	{
		url = ConfigReader.ConfigFileValue("qaenv");
		browser = ConfigReader.ConfigFileValue("browser");
		
		System.out.println("Browser -- > " + browser + ", URL  -- > " + url);
		
		driver = SetBrowser(browser);
		testReport = ExtentManager.getExtentReport();
	}
	
	@Test(groups = { "sprints"})
	public void demo()
	{
		driver.get(url);
		System.out.println("Testing demo() from TEST2");
	}
	
	@Test(groups = { "sanity"})
	public void masspayCheck()
	{
		driver.get(url);
		System.out.println("Testing masspayCheck() from TEST2");
	}
	
	@Test(groups = { "sprints"})
	public void bookmyshowCheck()
	{
		driver.get("http://www.bookmyshow.com");
		System.out.println("Testing bookmyshowCheck() from TEST2");
	}

	
	@AfterTest(alwaysRun=true)
	public void cleanUp()
	{
		driver.quit();
		ExtentManager.WriteToReport();
	}
}
