package tests.masspay;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import instarem.utilities.ConfigReader;
import instarem.utilities.Constants;
import instarem.utilities.ExtentManager;
import tests.base.TestBase;

public class Test1 extends TestBase {
	
	private static String browser, url;
	private static RemoteWebDriver driver=null;
	private static ExtentReports testReport=null;
	private static ExtentTest extentTest=null;
	
	
	@BeforeTest(alwaysRun=true)
	public void setup() throws Exception
	{
		url = ConfigReader.ConfigFileValue("qaenv");
		browser = ConfigReader.ConfigFileValue("browser");
		
		System.out.println("Browser -- > " + browser + ", URL  -- > " + url);
		
		driver = SetBrowser(browser);
		testReport = ExtentManager.getExtentReport();
		
	}
	
	@Test(groups= {"sanity"})
	public void testdemo()
	{
		driver.get(url);
		System.out.println("Testing testdemo() from TEST1");
	}
	
	@Test(groups= {"sprints"})
	public void RediffCheck()
	{
		driver.get("http://www.rediff.com");

		System.out.println("Testing RediffCheck() from TEST1");
	}

	@Test(groups= {"regression"})
	public void newsCheck()
	{
		driver.get("http://www.ndtv.com");

		System.out.println("Testing newsCheck() from TEST1");
	}
	
	@Test(groups= {"sanity"})
	public void amazonCheck()
	{
		driver.get("http://www.amazon.in");

		System.out.println("Testing amazonCheck() from TEST1");
	}
	
	@AfterTest(alwaysRun=true)
	public void cleanUp()
	{
		driver.quit();
		ExtentManager.WriteToReport();
	}
}
